const express = require('express');
const app = express();
const port = process.env.PORT || 3001;

// Route for api.hems.gov.mv/api/hi
app.get('/api/hi', (req, res) => {
    // Code to fetch data from http://localhost:3000/domainsData
    // ...
    // Return data as JSON
    res.json(data);
});

const jsonServer = require("json-server"); // importing json-server library
const server = jsonServer.create();
const router = jsonServer.router("db.json");
const middlewares = jsonServer.defaults();


server.use(middlewares);
server.use(router);

server.listen(port);

app.listen(port, () => {
    console.log(`Server listening on port ${port}`);
});



// const jsonServer = require("json-server"); // importing json-server library
// const server = jsonServer.create();
// const router = jsonServer.router("db.json");
// const middlewares = jsonServer.defaults();
// const port = process.env.PORT || 3001; // you can use any port number here; i chose to use 3001

// server.use(middlewares);
// server.use(router);

// server.listen(port);